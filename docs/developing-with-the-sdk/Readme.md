[Home](../../#advanced-developement-developing-new-apploications)

# Developing New Application with EZ-Connect-Lite

### Introduction
Applications are actual your interests, using this SDK and Development board you can create any application using the powerfull features of MW300. EZ-Connect Lite SDK contains several sample applications, that will help you to develop your own application. Few of these sample apps are explained in the [tutorial](../../docs/tutorials/) section.

Let's quickly look at how you can develop your own application with the SDK.

### Creating a new app in Eclipse
* Let's clone first any appliation from sample_apps to the new application that you wish to create, for ex say 'my_test_app". For this go to file explorer on your host, locate the EZ-connect-Lite sdk and copy sample app (for ex hello_world) folder and paste it as 'my_test_app' at the same location.
* open sample_apps/my_test_app/build.mk file by any editor and search and replace 'hello_world" with 'my_test_app' to prefare for your new applifcation build. after the change save the file.
* open a file sample_apps/sample_apps.mk file in the Eclipse IDE by double clicking it in the **Project Exporer** window and at the end append below line to it

```
subdir-y			 += sample_apps/my_test_app
```

* Then save the file. This will configure build system to build this new app along with other apps
* build the project again, at the end of the build you will find my_test_app.bin in bin/<board>/ folder.
* Now you can edit sample_apps/my_test_app/main.c for customisation as per new applicaiton rewuirements.

### Build Configuration for custom app
**OPTIONAL Setup :** As a part of stand build process all the supported sample apps are build. For your own custom app development, you may love to build the app of your interest (any one sample or custom app).

To configure build system for this objective, follow the process below.

* In this context, let's say you would like to configure build system to build your new app under development which is `my_test_app`.
* Go to Project → Properties → C/C++ Build → Environment.
* Click on Add button, enter in the Name field `APP` and in the Value field as `<path>/<appfolder>`(for ex: ./sample_app/my_test_app).
* Click on Apply to save it.
* Finally click on Ok to apply all changes.

If you wish to customize application build from command line interface, for similar configuration you can pass addtional build parameters to the make-

```
For Ex: make APP=./sample_app/my_test_app
```

### Constructing main.c
Let's begin with the constructing simple app. Here's sample code for the same :

```
int main(void)
{
        int count = 0;

        /* Initialize console on uart0 */
        wmstdio_init(UART0_ID, 0);

        wmprintf("My New MW300 Application Started\r\n");

        while (1) {
                count++;
                wmprintf("My New App: iteration %d\r\n", count);

                /* Sleep  5 seconds */
                os_thread_sleep(os_msec_to_ticks(5000));
        }
        return 0;
}
```

The `main()` function is the entry point for any application. This function is expected to perform any application specific initializations and launch any application threads.

The `wmstdio` facility can be used to print log messages on the serial console over UART. Before using this though, we will have to initialize the UART. This is done using the `wmstdio_init()` call. This function identifies the UART that should be used for this communication and the baud rate that should be used.

The `wmprintf()` function is then used to print messages to the console.

### Understanding Wi-Fi APIs
The first thing that you typically do with an IoT Kit is to get it on a network. The easiest way to connect to a known Wi-Fi network is to use the function [`wm_wlan_connect()`](../../refman/html/wmsdk_8h.html#abadf6262ff53dd041ebd0c3933bb2bdc). This function takes a Wi-Fi network name and a passphrase as an argument. It then attempts to connect to this Wi-Fi network.

The [`wm_wlan_connect()`](../../refman/html/wmsdk_8h.html#abadf6262ff53dd041ebd0c3933bb2bdc) is simple and useful, but what if you don't want to hard-code the network credentials in your application firmware's code? The SDK also offers another more powerful Wi-Fi API, [`wm_wlan_start()`](../../refman/html/wmsdk_8h.html#a487b6f0c6c72bd77453b110d9accb4f0). This API does a number of things,

- Firstly, it starts a Wireless Network of its own, a Micro-AP/uAP
- On this uAP network, it hosts a web-app based wizard that guides the end-user through the provisioning of the home Wi-Fi network
- The end-user can launch this web-app by connecting with the uAP network, and then launching a browser that points to `http://192.168.10.1`
- The end goal of this web-app is to retrieve a Wi-Fi network name and passphrase from the end-user
- Once this configuration is received, then the kit makes connection attempts to the configured network. Once a network is configured, the firmware remembers this configuration across power resets. Subsequent boot-ups of the kit, will skip the setup of the initial configuration wizard, but instead directly start making connection attempts with this network.

### Connectivity Callbacks
The application firmware gets to know the status of the Wi-Fi connectivity using connectivity callbacks. The following connectivity callbacks have been defined:

- [`wlan_event_normal_connected()`](../../refman/html/wmsdk_8h.html#a406020d6598caca9eb8074321ca7ccb1): This function is called whenever the Wi-Fi station successfully associates with the destined Wireless Access Point.
- [`wlan_event_connect_failed()`](../../refman/html/wmsdk_8h.html#a51ba3386516da0efca21ae06defe5636): This function is called whenever the Wi-Fi station fails to associate with the destined
- [`wlan_event_normal_link_lost()`](../../refman/html/wmsdk_8h.html#a41961638c0f6880ffdc32d1cc56caaa9): This function is called whenever an existing association with the Wireless Access Point is lost

An application firmware can define these functions to take the required action as is applicable to them.

####[Back to Main Menu](../../#advanced-developement-developing-new-apploications)
