# Interfacing Sensors for AWS IoT Demo

# Introduction and Overview
Welcome to Marvell's AWS IoT Starter Kit to Interface Grove Sensors
This documents is a quick start guide that helps to setup Grove sensors to work with Marvell's AWS IoT Starter Kit to report the sensor data to AWS cloud. Basic demo setup information in this document is around Temperature and Humidity sensor, you may further evaluate other supported sensors too.  

## H/W Overview
###[Marvell MW302 IoT Starter Kit Powered by AWS](http://www.amazon.com/Marvell-MW302-IoT-Starter-Powered/dp/B0168DLQHI)
This is the main board Powered by AWS IoT is available for sale globally on Amazon.com. All the sensors need to be interfaced to this board. There is not specific interreace for this mating so you will need to prepare interface wiring connections yourself.

![](http://www.marvell.com/microcontrollers/assets/MW302_Kit-sm1.jpg)

###[Marvell MW302 Based IoT Evaluation Kit by VADACTRO](http://www.edu.vadactro.org.in)
This development board is perfect choice for this demontration since the sensors can be interfaced cleanly with the system. Currently this board is not available for sale online, you need to contact VADACTRO for procurement. This document is created considering this board as a reference platform.

<img src="../vd-iot-evalkit/VD-IOT-EVALKIT-V12-Both-Small.jpg" width="700"></img>

### [BASE-SHILD: Base Shield V2](http://www.seeedstudio.com/depot/Base-Shield-V2-p-1378.html)
This BASE shield offers easy connections to the multiple sensors and it mates perfectly on the VD-IOT-EVALKIT connector interface.

<img src="https://statics3.seeedstudio.com/seeed/img/2016-09/oJYc6A5jTrgslQWC5j7gw9tI.jpg" width="500"></img>

### [Grove Environmental Sensors](http://www.seeedstudio.com/wiki/GROVE_System#Environmental_Monitoring)
[//]: #  (Some datasheets and images have no sources,ie:404)


| S.N. | Grove Sensor | Image | Datasheet | Wiki |
| ---- | --- | --- | --- | --- |
| 1. | [Grove - Temp&Humi Sensor](http://wiki.seeed.cc/Grove-TemperatureAndHumidity_Sensor/) | <img src="https://raw.githubusercontent.com/SeeedDocument/Grove-TemperatureAndHumidity_Sensor/master/img/Grove-TempAndHumiSensor.jpg" width="100"> | [Datasheet](https://raw.githubusercontent.com/SeeedDocument/Grove-TemperatureAndHumidity_Sensor/master/res/Temperature_Humidity.zip) | WiKi |
| 2. | [Grove - Barometer Sensor (BMP180)](http://www.seeedstudio.com/depot/Grove-Barometer-Sensor-BMP180-p-1840.html) | <img src="https://raw.githubusercontent.com/SeeedDocument/Grove-Barometer_Sensor-BMP180/master/img/Barometer-BMP180-.jpg" width="100"> | [Datasheet](http://www.seeedstudio.com/wiki/images/7/7e/BMP180.pdf) | [WiKi](http://wiki.seeed.cc/Grove-Barometer_Sensor-BMP180/) |
| 3. | [Grove - CO2 Sensor](http://www.seeedstudio.com/depot/Grove-CO2-Sensor-p-1863.html) | <img src="https://statics3.seeedstudio.com/images/product/Grove%20CO2%20Sensor.jpg" width="100"> | Datasheet | WiKi |
| 4. | [Grove - Gas Sensor(MQ9)](http://www.seeedstudio.com/depot/Grove-Gas-SensorMQ9-p-1419.html) | <img src="https://statics3.seeedstudio.com/images/101020045%201.jpg" width="100"> | Datasheet | WiKi |
| 5. | [Grove - Light Sensor](http://www.seeedstudio.com/depot/Grove-Light-Sensor-p-746.html) | <img src="https://statics3.seeedstudio.com/images/101020014%201.jpg" width="100"> | Datasheet | WiKi |
| 6. | [Grove - Temperature Sensor V1.2](http://www.seeedstudio.com/wiki/Grove_-_Temperature_Sensor_V1.2) |<img src="https://github.com/SeeedDocument/Grove-Temperature_Sensor_V1.2/raw/master/img/Grove_Temperature_Sensor_View.jpg" width="100"> | [Datasheet](http://www.seeedstudio.com/wiki/images/a/a1/NCP18WF104F03RC.pdf) | [WiKi](http://www.seeedstudio.com/wiki/Grove_-_Temperature_Sensor_V1.2) |
| 7. | [Grove_-_3-Axis_Digital_Accelerometer(±1.5g)](http://www.seeedstudio.com/depot/twig-3axis-accelerometer-p-765.html?cPath=144_146) | <img src="https://statics3.seeedstudio.com/images/101020039%201.jpg" width="100"> | [Datasheet](http://garden.seeedstudio.com/images/e/ee/MMA7660FC.pdf) | [WiKi](http://www.seeedstudio.com/wiki/Grove_-_3-Axis_Digital_Accelerometer(%C2%B11.5g)) |

# Assembling the Hardware

### List of minimum components to setup demo

| S.N. | Item | Image | Howto Get it |
| ---- | --- | --- | --- | --- |
| 1. | [Grove - Temp&Humi Sensor](http://wiki.seeed.cc/Grove-TemperatureAndHumidity_Sensor/) | <img src="https://raw.githubusercontent.com/SeeedDocument/Grove-TemperatureAndHumidity_Sensor/master/img/Grove-TempAndHumiSensor.jpg" width="100"> | [Order Online](https://www.seeedstudio.com/Grove-Temp%26Humi-Sensor-p-745.html) |
| 2. | [Grove - Base Shield V2)](https://www.seeedstudio.com/Base-Shield-V2-p-1378.html) | <img src="https://statics3.seeedstudio.com/seeed/img/2016-09/oJYc6A5jTrgslQWC5j7gw9tI.jpg" width="200"> | [Order Online](https://www.seeedstudio.com/Base-Shield-V2-p-1378.html) |
| 3. | [Marvell MW302 Based IoT Evaluation Kit by VADACTRO](http://www.edu.vadactro.org.in) | <img src="../vd-iot-evalkit/VD-IOT-EVALKIT-V12-Both-Small.jpg" width="200"> | [Contact Supplier](mailto:avinash@vadactro.org.in) |

### Connecting WiFi Board to the Debug Board
The debug board is needed only for the development purpose. Once the development is done, you may remove the debug board to deploy standalone system on the field for testing.

<img src="https://s17.postimg.org/kv0i8tpfj/Connecting_Debugger_to_Evalkit.jpg" width="500"></img>

### Connecting Base Shield to the VADACTRO's IoT Evaluation Kit
This is very simple. Just plug in the Base Shield on the Debug board. The following picture gives brief idea on making connections.

<img src="https://s13.postimg.org/f4iexxco7/Connecting_Base_Shield_to_Evalkit.jpg" width="500"></img>

All three boards connected and ready to use look like

<img src="https://s3.postimg.org/fb2gxbmgj/Evalkit_With_Base_Shieldjpg.jpg" width="500"></img>

With this interface IOs from MW300 on WiFi board will be get connected to the different sensor interface connectors available on the base shield. For the supported sensor to be interfaced here, the following table provides brief summary of IO allocations. This information is useful for the developer. You can find more details on interfacing by referring [Pin Map](../vd-iot-evalkit/VD-IOT-EVALKIT-V1P-PinMap-Small.jpg) details.

| S.N. | Signal | Connection on the BASE-SHIELD |
| ---- | --- | ---|
| 1. | 3.3V | BASE-SHIELD:J17:Pin5(3.3V) |
| 2. | 1WIRE (IO-00) | BASE-SHIELD:J18:Pin4(Analog-3) |
| 3. | GND | BASE-SHIELD:J17:Pin2(GND) | 
| 4. | UART2_RXD (IO-48) | BASE-SHIELD:J20:Pin2(TX) |
| 5. | UART2_TXD (IO-49) | BASE-SHIELD:J20:Pin1(RX) |
| 6. | GPIO47 (IO-47) | BASE-SHIELD:J18:Pin4(Analog-3) |
| 7. | GPIO42 (IO-42) | BASE-SHIELD:J18:Pin2(Analog-1) |
| 8. | GPIO43 (IO-43) | BASE-SHIELD:J18:Pin3(Analog-2) |
| 9. | SDA (IO-04) | BASE-SHIELD:J19:Pin9(SDA) |
| 10. | SCL (IO-05) | BASE-SHIELD:J19:Pin19(SCL) |

### Connecting Sensors to the Base Shield

<img src="https://s22.postimg.org/6sqc1jn1t/Connecting_Sensor_to_Base_Shield.jpg" width="500"></img>

The software in the SDK is written with respect to interface mentioned above. For faithful operation of the sensor, it is essential that you connect a sensor in the respective  connector interface on the Base Shield. The following table helps to identify the socket for the right sensor:
 
| S.N. | Grove Sensor | Socket on the Base Shield |
| ---- | --- | ---|
| 1. | Grove - Temp&Humi Sensor | Connect on A3 socket |
| 2. | Grove - Barometer Sensor (BMP180) | Connect on any of I2C socket |
| 3. | Grove - CO2 Sensor | Connect on UART socket |
| 4. | Grove - Gas Sensor(MQ9) | Connect on A3 socket |
| 5. | Grove - Light Sensor | Connect on A2 socket |
| 6. | Grove - Temperature Sensor | Connect on A0 socket |
| 7. | Grove - 3-Axis_Digital_Accelerometer(±1.5g) | Connect on any of I2C socket |

The following picture show all the sensors connected to the Base Shield

<img src="https://s15.postimg.org/8plhxwq17/Evalkit_with_base_Shield_Debug_B_Sensor_Connected.jpg" width="500"></img>

#### Make sure the power switch on the Base Shield is selected to use 3.3V power.

![](https://s12.postimg.org/60nfymjsd/Base_Shield_3_VSwitch_Postion.png)

Please note that Base Shield can supply both (3.3V and 5V) power to the 4-Pin Sensor connectors, but the MCU on WiFi board is 3.3V powered. So, it is mandatory to interface sensor IOs at the right logic level (0-3.3V) to the MCU. If this power switch is selected for 5V, it will supply 5V to the connector and the sensor connected to the interface may provide incompatible logic level and that may damage the MCU on the WiFi board.

So please make sure this switch is configured to supply 3.3V.

With this, your hardware is ready to use. The next step is to prepare a development setup and flash the relevant software on the board.


# Software (Download, Build, Flash and Run)
### Development Setup with the host
 Connect VD-IoT Eval **Debugger Board** to the Linux host using mini-USB cable to the JTAG-UART USB interface on the debugger board. This will power up the board.

### Preparing the EZ-Connect Lite SDK with sensor support:

Follow instruction in [Get The SDK](http://marvell-iot.github.io/docs/#documentation-get-the-sdk) chapter to get the software

### Building Firmware binaries with Grove sensor support

1. Once you git pull the source code, switch to the folder

                 cd ez-connect-lite

1. Clean the project binaries (Object Files created earlier)  

                 make clean

1. To **Build** the firmware for required sensor support,you need to specifically pass an additional argument (BOARD=vd_iot_evalkit-v1p and SEN_XX=y) to the build system. Because,the default SDK is not configured to build firmware for the board being used here. For ex: to build the projetct for Temperature and Humidity sensor support the command is- 

                 make BOARD=vd_iot_evalkit-v1p XIP=1 SEN_TH=y

1. You can customize a build for selecting some other sensor support. For example,
 if you wish to build the firmware for Pressure Sensor, then you should use-

                 make BOARD=vd_iot_evalkit-v1p XIP=1 SEN_PRESSURE=y

As a result of successful build, you will get binaries to flash on the board.

### Flashing firmware binaries on VD-IoT Eval Kit :
####  Flash the layout
Flash the layout and set up flash partition table.This is one time activity to be performed at the beginning of your project.For your subsequent updates you don't need to flash 'layout.txt' again ,unless you wish to erase entire flash memory and pre-configuration data.

         ./sdk/tools/OpenOCD/flashprog.py -l ./sdk/tools/OpenOCD/mw300/layout.txt

Note: If above command doesn't work on your host, then try executing it with root privileges (sudo)
>         sudo ./sdk/tools/OpenOCD/flashprog.py -l ./sdk/tools/OpenOCD/mw300/layout.txt 


#### Flash newly build firmware binaries
Then flash newly build firmware binaries using the following command:

	./sdk/tools/OpenOCD/flashprog.py --boot2 ./boot2/boot2.bin --mcufw ./bin/vd_iot_evalkit-v1p/aws_starter_demo.bin --ftfs ./bin/vd_iot_evalkit-v1p/aws_starter_demo.ftfs --wififw ./wifi-firmware/mw30x/mw30x_uapsta_14.76.36.p103.bin.xz

Note: If above command doesn't work on your host, then try executing it with root privileges (sudo).

### Re-flashing firmware involving only minor changes during development
You may frequently need to make some minor modification to the code and test the result.In such case,you don't need to flash all the firmware binaries every time to the board. You have to re-flash only MCU firmware binary. This can be done by using the following command:

	./sdk/tools/OpenOCD/flashprog.py --mcufw ./bin/vd_iot_evalkit-v1p/aws_starter_demo.bin

#### Test the board with new firmware
Reboot or re-power the board. You will find an Orange LED flashing on the board slowly. This indicates that the board is now ready to use. 

Using debug console you may validate the build version and build date to cross check your newly build firmware is active and running.

### Configuring the board for AWS IoT Connectivity
Follow the documentation [here](https://www.google.co.in/url?sa=t&rct=j&q=&esrc=s&source=web&cd=2&cad=rja&uact=8&ved=0ahUKEwjf073pze7ZAhUJMY8KHe4aBF8QFggtMAE&url=https%3A%2F%2Fdocs.aws.amazon.com%2Flambda%2Flatest%2Fdg%2Fsetup.html&usg=AOvVaw1uTKFNbZUiV9OcSLk1pzad) to setup your AWS account, IOT things and setup the kit to establish connection.

Once the successful connection is established with the AWS IoT Infrastructure, the sensors will start pushing the information to the cloud.

Now you can disconnect your board from the debugger board and do the standalone testing.
 
### Executing and testing the results
#### Powering up the board
Power up the VD-IoT-EvalKit board using standard Micro-USB connector.

#### Seeing the results on debug console:
1. Hook up any serial console (like minicom, or hyperterminal ) to see the debug logs from the board (Serial Port Configuration:115200/8N1).
1. Re-power up or reset the board, this will reboot the system and you will see the sensors events being generated and posted to the AWS cloud on your debug console.
1. Please note that scanning a sensor and sending a relevant data to the AWS cloud will be only done if your board is successfully connected to the AWS cloud.
1. Using debug console you can see what data is being generated and posted to the AWS cloud
1. Here are some sample test results captured from the debug console for your reference:

[//]: # (Log files missing not available on given links:VALIDITY Lapsed )

* [Test log for ADC based Temperature Sensor (make SEN_TEMPR=y)](http://s000.tinyupload.com/?file_id=05823520305774658906)
* [Test log for ADC based Light Sensor (make SEN_LIGHT=y)](http://s000.tinyupload.com/?file_id=03962066528218263080)
* [Test log for ADC based Gas Sensor (make SEN_GAS=y)](http://s000.tinyupload.com/?file_id=00512649497357702662)
* [Test log for I2C based Pressure Sensor (make SEN_PRESSURE=y)](http://s000.tinyupload.com/?file_id=04818343977242733060)
* [Test log for I2C based 3-Axis_Digital_Accelerometer Sensor (make SEN_ACC=y)](http://s000.tinyupload.com/?file_id=05905979737430672832)
* [Test log for UART based CO2 Sensor (make SEN_CO2=y)](http://s000.tinyupload.com/?file_id=03351349396714574498)
* [Test log for 1WIRE Protocol based Temperature&Humidity Sensor (make SEN_TH=y)](http://s000.tinyupload.com/?file_id=50250452625542871097)

#### Seeing the results on AWS IoT
Please refer [**Working with Thing Shadows (Simulating an app)**](http://marvell-iot.github.io/docs/aws-iot/#-working-with-thing-shadows-simulating-an-app) Section in the Getting Started Guide.

# Software, Design and Architecture
## Software
### Marvell EZ-Connect Lite SDK:
Developers can get started with the [EZ-Connect Lite SDK](https://github.com/marvell-iot/ez-connect-lite) available at gitlab. The starter SDK will allow developers to quickly prototype their IoT device concept by using the IO interfaces available on the development kit. The SDK includes libraries to connect the kit to a Wi-Fi network and then establish a secure, bi-directional communication to the AWS cloud and interface to the various IoT related services offered by AWS

#### What is the sample app? What does the sample app do?
 [//]: #  (remove MARVEL here?)

**Sample apps** are simple applications provided in the Marvell EZ-Connect Lite SDK, that help developers to understand how to use available H/W IPs on MW300 development platform. There are several apps bundled with the SDK including **aws_starter_demo**. A sample app provides guidelines on  how to create a custom application for specific need using different software features provided in the SDK.

#### How do you use it with AWS IoT Service?
**aws_starter_demo** is one of the apps bundled in the SDK that can be used to connect a device- "Thing"-to the AWS-IoT Cloud infrastructure through WiFi communication channel through MW300. You can find more detailed documentation on this [here](http://marvell-iot.github.io/docs/aws-iot/).
 
## System Architecture for Sensor Interface
[AWS-IoT](http://docs.aws.amazon.com/iot/latest/developerguide/what-is-aws-iot.html) provides a standard framework to exchange the event notification across the devices through cloud. you can find more information about AWS IoT System architecture [here](http://docs.aws.amazon.com/iot/latest/developerguide/aws-iot-how-it-works.html). [Marvell EZ-Connect Lite](https://github.com/marvell-iot/ez-connect-lite) provides sample applications that may help you to understand how to handle H/W IPs of MW300 like UART, GPIOS, I2C, SPI to communicate with sensor hardware. It also contains sample application to post the push button sensory events to the AWS cloud. It contains all necessary software for MW300 to build, debug and flash your custom application on the target board. More details on this can be found [here](../../#quick-start-guide-4-build-sample-applications).

![](http://s17.postimg.org/cnj0g5qzj/sensorinterface_layer_block_diag.jpg)

### Sensor Interface Layer.
Each sensor is different of its kind and is a transducer that converts sensory information into the electrical signal (digital or analog). These sensors are interfaced with the MW300 through any of available H/W Input Pins on the board (for ex: GPIO, ADC, I2C etc...).The code that grabs the information from the sensor is hardware specific (we call it as a **Sensor Low Level Driver**). It interacts with the sensor H/W for initialization and configuration and then further to read sensor data periodically. **Sensor Interface Layer** is a **middleware**, a thin layer of interface between main application layer and the multiple sensors supported through sensor low level drivers.  

**Sensor Interface Layer** provides APIs to be used in the main application and the hooks to interface. 
#### Sensor Interface Layer APIs

| S.N. | API | Description |
| ---- | --- | ---| --- | ---|
| 1. | int sensor_drv_init(void) | This API must be called once during initialization by the application layer |
| 2. | int sensor_event_register(struct sensor_info *sevnt) | This API is used to register Sensor Low Level Driver with the Sensor interface layer. For each sensor to be supported, there must be one register call. |
| 3. | int sensor_msg_construct(char *src, char*dest, int len) | Generally this API is called from the **Publish thing state to shadow** context of AWS IoT application. If there is a difference  observed between two successive sensor readings, a JSON object will be constructed to post over AWS-IoT Cloud | 
| 4. | void sensor_inputs_scan(void) | This API is used to perform Sensor Scan operation. All the registered sensors will be scanned in a sequence. |
| 5. | int sensor_event_unregister(struct sensor_info *sevnt) | Not implemented yet, reserved for future use cases |

### Sensor Low level Driver
The interface is very flexible so that you can register multiple sensors seamlessly to initialize, scan and report multiple sensory events using JSON object. It also provides an abstraction layer for Sensor Low Level Driver and rest of the software for better code management.

In brief, a sensor developer should only worry and focus on the sensor development and achieving desired accuracy and he should just delegate all communication related dependencies and handling to the upper layers. 

**Sensor Low level Driver** is nothing but a set of one c file and one header file that has mostly sensor H/W specific code. It has mainly register, initialize and scan APIs to interface with upper layer. Then it can have the code specific to the sensor to capture the sensory information, that may vary from case to case.

#### Sensor Low Level Driver Data structure

                 `/* Struct to hold each sensor event data */`
                 `struct sensor_info {`
                 	`char event_curr_value[64];`
                 	`char event_prev_value[64];`
                 	`char *property; /*JSON Name of event */`
                 	`/* Sensor Low Level Driver Interfacing APIs */ `
                 	`int (*init)(struct sensor_info *sevent);`
                 	`int (*read)(struct sensor_info *sevent);`
                 	`int (*write)(struct sensor_info *sevent);`
                 	`int (*exit)(struct sensor_info *sevent);`
                 	`struct sensor_info *next;`
                 `};`

#### Sensor Low Level Driver APIs
Sensor Low Level Driver Data structure provides a call-back mechanism through function points to be executed by Sensor Interface Layer whenever is needed.
 
| S.N. | API | Description |
| ---- | --- | ---| --- | ---|
| 1. | int (*init)(struct sensor_info *sevent) | This is a callback function, will be executed by the Sensor Interface Layer in the context of sensor_event_register API; All H/W initialization to be performed in this callback |
| 2. | int (*read)(struct sensor_info *sevent) | This is a callback function, will be executed by the Sensor Interface Layer in the context of sensor_inputs_scan operation, generally sensor is polled through this API to read its value periodically |
| 3. | int (*write)(struct sensor_info *sevent) | Reserved for future use cases | 
| 4. | int (*exit)(struct sensor_info *sevent) | Reserved for future use cases |

You can create such pair of file for each driver to be supported, the sample code templates are as follows:

[sensor_template_drv.c](https://github.com/marvell-iot/ez-connect-lite/blob/master/sdk/src/drivers/peripherals/sensors/src/sensor_template_drv.c)

[sensor_template_drv.h](https://github.com/marvell-iot/ez-connect-lite/blob/master/sdk/src/drivers/peripherals/sensors/src/sensor_template_drv.c)

There are few Grove sensor already supported in the SDK, you can find them [here](https://github.com/marvell-iot/ez-connect-lite/tree/master/sdk/external/sensors/src/grove).
[//]: # (File not found)

### Support
The support is available on email or chat at github (https://github.com/marvell-iot)
[//]: # (Replacement with gitlab/vdedu or mail ID right?)

### TODO List
1. Low Level Drivers for Light sensor, NTC based temperature sensors and Gas sensors are interfaced on ADC channels, and those needs calibration for desired level of accuracy for the readings which are not done in the current implementation. This need to be addressed with some reference source for cross verification.

