[Home](../../#quick-start-guide-1-get-the-development-boards)

# AWS IoT Starter Kit
<a href="https://www.amazon.com/Globalscale-MW302-IoT-Starter-Powered/dp/B0168DLQHI/" target="_blank" class="button">Buy Now</a>

### Introduction

[Marvell MW302 IoT Starter Kit Powered by AWS](http://www.amazon.com/Marvell-MW302-IoT-Starter-Powered/dp/B0168DLQHI)
The kit is powered by Marvell EZ-Connect MW300/302 Wi-Fi microcontroller system-on-chip (SoC), a single-chip SoC with 1x1 802.11n Wi-Fi and full-featured Cortex-M4 microcontroller.  The SoC includes 512kB SRAM and a flash controller to enable executing code from external QSPI Flash. The SoC also enables easy interfacing to sensors, actuators, and other components via a full set of I/O interfaces including SPI, I2C, UART, I2S, PWM, ADC, DAC etc.  The development kit includes a set of IO headers that bring out these interfaces to connect to external sensor or other peripheral boards.

![](http://www.marvell.com/microcontrollers/assets/MW302_Kit-sm1.jpg)

### Board Schematics

Download board schematics from [here](./MV-SR00281-01.pdf)

### Jumper Settings
You can locate the jumper location on the board using below diagram

<a href = "./MW300_RD_Jumper_diagram.png"> <img src="./MW300_RD_Jumper_diagram.png" width=100%> </a>

Jumpers and their usage description

<a href = "./MW300_RD_Jumper_settings.png"> <img src="./MW300_RD_Jumper_settings.png" width=100%> </a>

### Borad Swtiches

There are four configurable swiches on the board, of which two are already configured in the SDK and can be used by end application for any need.
 
| SWITCH  | GPIO Used | Usage |
| :------------ |:---------------:| :-----:|
| SW1     | GPIO_24 | board_button_1() |
| SW2     | GPIO_26 | board_button_2() |
| S1     | RESET | System Reset |
| SW4     | GPIO_22 | not used in SDK |
| SW5     | GPIO_23 | not used in SDK |

### Board LEDs

You can use below LEDs in your application for indication purpose, those are configured in the SDK as explained below

| LED  | GPIO Used | Usage |
| :------------ |:---------------:| :-----:|
| D7(Yellow)     | GPIO_41 | board_led_1() |
| D8(Amber)     | GPIO_40 | board_led_2() |

### Pin Map

**NOTE** : The Starter Kit Board is 3.3v tolerant. If you are using a 5v sensor or module, please make sure that you shift down the voltage levels using a resistances or a level-shifter IC. **5v input can damage the module**.

<a href = "./PinMap.png"> <img src="./PinMap.png" width=75%> </a>

(click image to open larger version)

<strong>Header 2</strong>

To the left of the SoC

| GPIO_#  | Function 0  | Function 1 | Function 2 |
| :------------ |:---------------:| :-----:| :-----:|
| 11      | UART1_CTS | SSP1_CLK|-
| 12      | UART1_RTS |SSP1_FRM |-
| 04      | I2C0_SDA | -|GPT0_CH4
| 05      | I2C0_SCL | -|GPT0_CH5
| 13      | UART1_TXD| SSP1_TXD|-
| 14      | UART1_RXD | SSP1_RXD| -
| 22      | WAKE0 | - | -
| 26      | PUSH_SW0 |I2C1_SCL|-
| 25      | 32KHz_CLK_IN | I2C1_SDA|-
| 39      | I2C0_RESET | - | -
| 23      | WAKE1 |- |-
| 02      | UART0_TXD | SSP0_TXD|GPT0_CH2
| 03      | UART0_RXD | SSP0_RXD|GPT0_CH3
| 16      | STRAP1 |- |-
| 17      | GPT3_CH0 |I2C1_SCL |-
| 18      | GPT3_CH1 | I2C1_SDA|-
| 24      | PUSH_SW1 | GPT1_CH5|-

<strong>Header 4</strong>
To the right of the SoC

| GPIO_#  | Function 0  | Function 1 | Function 2 |
| :------------ |:---------------:| :-----:| :-----:|
| 0      | UART0_CTS | SSP0_CLK| GPT0_CH0
| 01      |  UART0_RTS|SSP0_FRM |GPT0_CH1
| 40      |  LED1|- |-
| 41      | LED2 | -|-
| 27      | STRAP0 |USB_DRVBUS|-
| 48      | SSP2_TXD | UART2_TXD |ADC0_6
| 49      | SSP2_RXD | UART2_RXD|ADC0_7
| 47      | SSP2_FRM | UART2_RTS|ADC0_5
| 42      | ADC0_0 | ACOMP0|-
| 43      |  ADC0_1| ACOMP1|-
| 46      |  SSP2_CLK| UART2_CTS|ADC0_4


### SDK Build customization

This is default configured board in the EZ Connect Lite SDK , and the associated board file is `mw302-rd.c`. If you do not explicitly mention a  `BOARD_FILE` while building the SDK or a single `APP`, it will assume it is the this board AWS Starter Kit.

Here's how to compile a simple `hello_world` for AWS Starter Kit -

```
make APP=sample_apps/hello_world
```

### Board binaries
The generated binaries can be found in the `bin/mw302_rd`


####[Back to Main Menu](../../#quick-start-guide-1-get-the-development-boards)

