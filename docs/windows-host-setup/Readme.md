[Home](../../#quick-start-guide-2-setup-development-host)

# Setup Development Environement on Windows

### Installing Packages
* Install the following GnuWin32 packages from http://gnuwin32.sourceforge.net/packages.html:

 * [coreutils](http://gnuwin32.sourceforge.net/downlinks/coreutils.php)
 * [make](http://gnuwin32.sourceforge.net/downlinks/make.php)
 * [which](http://gnuwin32.sourceforge.net/downlinks/which.php)

  Add GnuWin32 installation path to the system path. You can find guidelines [here](https://stackoverflow.com/questions/23400030/windows-7-add-path).
* Install Python 2.7 for Windows from https://www.python.org/. Make sure python is added to the system path during installation. You can get installation process [here](https://www.howtogeek.com/197947/how-to-install-python-on-windows/).

### Installing WinUSB Driver
* Download WinUSB driver installer from http://zadig.akeo.ie/. If latest download fails on your system, try installing lower version.
* Connect the starter kit to the PC, let default Windows driver install first. After this,
   * Launch Zadig WinUSB driver installer
   * Click on options->List all Devices
   * Select Dual RS232 (Interface 0) in the dropdown list. Please note that the development board must be connected to your host while performing this step. The above interface will be detected by the system only if the target board is connected.
   * Select the driver WinUSB for this device
   * Install the driver

### Getting Serial Console
Once the appropriate drivers are installed and the development board is connected to your Windows host, virtual COM port devices are added to your systems. Depending upon your Windows configuration, you may see one or two COM ports. These can be verified as follows:

- Right click on My Computer and go to Properties
- Hardware --> Device Manager
- Under "Ports (COM & LPT), you may find the entries of the form "USB Serial Port (COMx)"
- If you dont find COMx entries then you may need to install FTDI VCP driver on your host, please follow the process [here](http://www.ftdichip.com/Drivers/VCP.htm) to get VCP driver installed.
- Please note that you will observe two USB Serial ports exposed (for ex: COM10, COM11), please use higher port for serial communication (for ex COM11)

This can be used to access the serial console of the development board.

The serial port can be accessed using any serial communication application like HyperTerminal or [Putty](https://www.putty.org/). Please perform the following settings for your serial console application:
```
Device: "USB Serial Port (COMx)"
Bits per Second: 115200
Data Bits: 8
Parity: N
Stop Bits: 1
Flow Control: None
```
### Install ARM Cross-compiler Toolchain

Apart from installing development host specific drivers and packages as mentioned above, an ARM cross-compiler toolchain should be installed on the development host. The EZ-Connect Lite SDK supports the ARM GCC Compiler Toolchain.

The toolchain for GNU ARM is available from: https://launchpad.net/gcc-arm-embedded/+download .
We have tested the latest release using tooldhain version [5-2016-q2-update](https://launchpad.net/gcc-arm-embedded/5.0/5-2016-q2-update) so we recommend to use the same. Download windows installer and install the toolchain.

**NOTE** : Please note the installation path, you may need it to update 'Path" environment variable on your system, on most of the system it is automatically updated as a part of installation process.

### Install Java (JDK and JRE)

* Download and install 1.8 or latter verion of Java for your OS. [Install Java](http://www.java.com/inc/BrowserRedirect1.jsp?locale=en)

####[Back to Main Menu](../../#quick-start-guide-2-setup-development-host)

