var searchData=
[
  ['os_5fget_5fruntime_5fstats',['os_get_runtime_stats',['../wm__os_8h.html#aa30c882d51b5955bf9b4a7d4b922d85f',1,'wm_os.h']]],
  ['os_5fmem_5falloc',['os_mem_alloc',['../wm__os_8h.html#a441855a588418a4f65adfc3b297db0ba',1,'wm_os.h']]],
  ['os_5fmem_5ffree',['os_mem_free',['../wm__os_8h.html#a06e61f4155c2d419601b47d4c45ebc1b',1,'wm_os.h']]],
  ['os_5fmem_5frealloc',['os_mem_realloc',['../wm__os_8h.html#ab2ff9d6c6795169698f8a8398c722fcb',1,'wm_os.h']]],
  ['os_5fmutex_5finherit',['OS_MUTEX_INHERIT',['../wm__os_8h.html#a9b148432f39b4c957cf41e5dc14b5f89',1,'wm_os.h']]],
  ['os_5fmutex_5fno_5finherit',['OS_MUTEX_NO_INHERIT',['../wm__os_8h.html#a6159b01fc9c098493b5172d10a901333',1,'wm_os.h']]],
  ['os_5fno_5fwait',['OS_NO_WAIT',['../wm__os_8h.html#ad33d01b851bcd653bbc8b564640d77c7',1,'wm_os.h']]],
  ['os_5fqueue_5fpool_5fdefine',['os_queue_pool_define',['../wm__os_8h.html#a72cca20d893f1cefe15ebfb0fc1ceea5',1,'wm_os.h']]],
  ['os_5fthread_5frelinquish',['os_thread_relinquish',['../wm__os_8h.html#a28697828e7f393a991d3a0be35478048',1,'wm_os.h']]],
  ['os_5fthread_5fstack_5fdefine',['os_thread_stack_define',['../wm__os_8h.html#a4a9cd42cccd68b3618cd0d20995a1b0f',1,'wm_os.h']]],
  ['os_5fticks_5fto_5funblock',['os_ticks_to_unblock',['../wm__os_8h.html#a29c9ba863f0414cfc7eb8ba36ab9c2d8',1,'wm_os.h']]],
  ['os_5fwait_5fforever',['OS_WAIT_FOREVER',['../wm__os_8h.html#a657765067071658ae82fdd3823fd06e5',1,'wm_os.h']]]
];
