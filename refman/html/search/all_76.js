var searchData=
[
  ['val',['val',['../structio__gpio__cfg.html#a88cace760cb6c684ae337936ea253653',1,'io_gpio_cfg']]],
  ['verify_5fsequential_5fpattern',['verify_sequential_pattern',['../wm__utils_8h.html#afd397db24261dab45dc7c859d73869ff',1,'wm_utils.h']]],
  ['version_5fmajor',['VERSION_MAJOR',['../aws__iot__version_8h.html#a1a53b724b6de666faa8a9e0d06d1055f',1,'aws_iot_version.h']]],
  ['version_5fminor',['VERSION_MINOR',['../aws__iot__version_8h.html#ae0cb52afb79b185b1bf82c7e235f682b',1,'aws_iot_version.h']]],
  ['version_5fpatch',['VERSION_PATCH',['../aws__iot__version_8h.html#a901edadf17488bb6be1ac9a1e3cfea7a',1,'aws_iot_version.h']]],
  ['version_5ftag',['VERSION_TAG',['../aws__iot__version_8h.html#aed90ca0652e9fe4d4876be6bb37c1c22',1,'aws_iot_version.h']]]
];
