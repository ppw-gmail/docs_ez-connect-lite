var indexSectionsWithContent =
{
  0: "_abcdefghijklmnoprstuvw",
  1: "_fijnostw",
  2: "abfglmnptw",
  3: "abcdefghijlmoprsuvw",
  4: "acdefgikmprstvw",
  5: "cfgjnorw",
  6: "bcfijlosu",
  7: "abdfijlmnostu",
  8: "abdgimnopsuv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

