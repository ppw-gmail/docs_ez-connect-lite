var searchData=
[
  ['os_5fdisable_5fall_5finterrupts',['os_disable_all_interrupts',['../wm__os_8h.html#a0b9b50d253288773c083ccfb52639f89',1,'wm_os.h']]],
  ['os_5fdump_5fmem_5fstats',['os_dump_mem_stats',['../wm__os_8h.html#aeacea2f48e66854206f532991bc5306a',1,'wm_os.h']]],
  ['os_5fenable_5fall_5finterrupts',['os_enable_all_interrupts',['../wm__os_8h.html#a96399efeef04926032c38be588f480c8',1,'wm_os.h']]],
  ['os_5fevent_5fnotify_5fget',['os_event_notify_get',['../wm__os_8h.html#aba070b81b50684c2c498b77c73417cea',1,'wm_os.h']]],
  ['os_5fevent_5fnotify_5fput',['os_event_notify_put',['../wm__os_8h.html#a7d29c10b933be4ecf62905a8d1ae9c81',1,'wm_os.h']]],
  ['os_5fget_5ftimestamp',['os_get_timestamp',['../wm__os_8h.html#a256026b9806e70d8a637a6c8830b7f3a',1,'wm_os.h']]],
  ['os_5fget_5fusec_5fcounter',['os_get_usec_counter',['../wm__os_8h.html#ace27a184c51a3e8107ea596788b2c3b7',1,'wm_os.h']]],
  ['os_5fheap_5fadd_5fbank',['os_heap_add_bank',['../wm__os_8h.html#a78ceeb26bfc6f528571aae4d3aaeee43',1,'wm_os.h']]],
  ['os_5fmem_5fcalloc',['os_mem_calloc',['../wm__os_8h.html#afc7dfc43115e762e89bd1e0e7a23d859',1,'wm_os.h']]],
  ['os_5fmem_5fget_5ffree_5fsize',['os_mem_get_free_size',['../wm__os_8h.html#a07c70bac5e35982ef0ecee662fd93c20',1,'wm_os.h']]],
  ['os_5fmsec_5fto_5fticks',['os_msec_to_ticks',['../wm__os_8h.html#a2f0d7a4f57afe10b6091032aa8ebff1e',1,'wm_os.h']]],
  ['os_5fmutex_5fcreate',['os_mutex_create',['../wm__os_8h.html#a55248be1d0f1dc18cc55354e208c99d0',1,'wm_os.h']]],
  ['os_5fmutex_5fdelete',['os_mutex_delete',['../wm__os_8h.html#a769e63abb2d7f2576a84ec51c1358117',1,'wm_os.h']]],
  ['os_5fmutex_5fget',['os_mutex_get',['../wm__os_8h.html#ab1ca2c6d5ddf6aff159c86429fa74744',1,'wm_os.h']]],
  ['os_5fmutex_5fput',['os_mutex_put',['../wm__os_8h.html#a12b9df5754eaa7e56ee3b34997f9fb8f',1,'wm_os.h']]],
  ['os_5fqueue_5fcreate',['os_queue_create',['../wm__os_8h.html#a2ecc0acf1e2d0632263a881af0c47209',1,'wm_os.h']]],
  ['os_5fqueue_5fdelete',['os_queue_delete',['../wm__os_8h.html#ac61cd752c7718dd6434363b706d07ff9',1,'wm_os.h']]],
  ['os_5fqueue_5fget_5fmsgs_5fwaiting',['os_queue_get_msgs_waiting',['../wm__os_8h.html#a73f448e2d24a8dfbd30f8e2742f8ec6d',1,'wm_os.h']]],
  ['os_5fqueue_5frecv',['os_queue_recv',['../wm__os_8h.html#a0c55cc70072431881d7a3f9b3dd8b8b9',1,'wm_os.h']]],
  ['os_5fqueue_5fsend',['os_queue_send',['../wm__os_8h.html#a42cc45755168752061d1c077ecdb0864',1,'wm_os.h']]],
  ['os_5frecursive_5fmutex_5fcreate',['os_recursive_mutex_create',['../wm__os_8h.html#af55280b947d7e2430932a49e4156d9a7',1,'wm_os.h']]],
  ['os_5frecursive_5fmutex_5fget',['os_recursive_mutex_get',['../wm__os_8h.html#ab5f7f10e2ba1f5616aff23d78c03f3de',1,'wm_os.h']]],
  ['os_5frecursive_5fmutex_5fput',['os_recursive_mutex_put',['../wm__os_8h.html#a8d16cf7a63bf029481abcb1179100313',1,'wm_os.h']]],
  ['os_5fremove_5fidle_5ffunction',['os_remove_idle_function',['../wm__os_8h.html#a95b2986a97dfb8321ef0e176ea3baa8a',1,'wm_os.h']]],
  ['os_5fremove_5ftick_5ffunction',['os_remove_tick_function',['../wm__os_8h.html#a45e956e7f0b646b8f4ec1c037ac57021',1,'wm_os.h']]],
  ['os_5frwlock_5fcreate',['os_rwlock_create',['../wm__os_8h.html#a00ea732b1319f0a612ae6a8661834261',1,'wm_os.h']]],
  ['os_5frwlock_5fdelete',['os_rwlock_delete',['../wm__os_8h.html#a62514c47a08858a5cd4bc7e064004918',1,'wm_os.h']]],
  ['os_5frwlock_5fread_5flock',['os_rwlock_read_lock',['../wm__os_8h.html#a3dc903e5d57ae858286001ae4bace36b',1,'wm_os.h']]],
  ['os_5frwlock_5fread_5funlock',['os_rwlock_read_unlock',['../wm__os_8h.html#adaf54a8ab1c7a4319d3472c2fe9d9962',1,'wm_os.h']]],
  ['os_5frwlock_5fwrite_5flock',['os_rwlock_write_lock',['../wm__os_8h.html#a0e15993c42e9c95fbe6f04b8a0f6d89b',1,'wm_os.h']]],
  ['os_5frwlock_5fwrite_5funlock',['os_rwlock_write_unlock',['../wm__os_8h.html#a9551bfba3c65aac192ad3fa3248e832e',1,'wm_os.h']]],
  ['os_5fsemaphore_5fcreate',['os_semaphore_create',['../wm__os_8h.html#a04114de41c85b27d657e96ddfd6c96e8',1,'wm_os.h']]],
  ['os_5fsemaphore_5fcreate_5fcounting',['os_semaphore_create_counting',['../wm__os_8h.html#a2c922506a76f0072d93cda780e36f57a',1,'wm_os.h']]],
  ['os_5fsemaphore_5fdelete',['os_semaphore_delete',['../wm__os_8h.html#a3144d44b6ad0c0bee22466c778d3cd23',1,'wm_os.h']]],
  ['os_5fsemaphore_5fget',['os_semaphore_get',['../wm__os_8h.html#acc49decee9819a251ddebf1839325660',1,'wm_os.h']]],
  ['os_5fsemaphore_5fgetcount',['os_semaphore_getcount',['../wm__os_8h.html#a3372eee0bd9e6752e25b9baa8f00146a',1,'wm_os.h']]],
  ['os_5fsemaphore_5fput',['os_semaphore_put',['../wm__os_8h.html#aa1775e425d295dff6777aa5459055054',1,'wm_os.h']]],
  ['os_5fsetup_5fidle_5ffunction',['os_setup_idle_function',['../wm__os_8h.html#a91b680f72f905326bdebab85a2a919e0',1,'wm_os.h']]],
  ['os_5fsetup_5ftick_5ffunction',['os_setup_tick_function',['../wm__os_8h.html#ae9a81d4d5e7703bc6cc31693da2cf305',1,'wm_os.h']]],
  ['os_5fthread_5fcreate',['os_thread_create',['../wm__os_8h.html#a869780b76f8c33a39a8de9ea97161218',1,'wm_os.h']]],
  ['os_5fthread_5fdelete',['os_thread_delete',['../wm__os_8h.html#a6f0b740285e068b29e99ccde30d1e7a7',1,'wm_os.h']]],
  ['os_5fthread_5fself_5fcomplete',['os_thread_self_complete',['../wm__os_8h.html#aaded94f2c3d37248f5b291725725945d',1,'wm_os.h']]],
  ['os_5fthread_5fsleep',['os_thread_sleep',['../wm__os_8h.html#a0dae29f732a3889b6e6a962d986a4eb2',1,'wm_os.h']]],
  ['os_5fthread_5fwait_5fabort',['os_thread_wait_abort',['../wm__os_8h.html#a139fc9c80fa48269806e6c6bceb585ff',1,'wm_os.h']]],
  ['os_5fticks_5fget',['os_ticks_get',['../wm__os_8h.html#a4637bca970c886d6adfdeab30c020e42',1,'wm_os.h']]],
  ['os_5fticks_5fto_5fmsec',['os_ticks_to_msec',['../wm__os_8h.html#ae6457c09e733cb892f62542fd33ed1c3',1,'wm_os.h']]],
  ['os_5ftimer_5factivate',['os_timer_activate',['../wm__os_8h.html#a08e21edab486b3db0f4e32153c1a85fc',1,'wm_os.h']]],
  ['os_5ftimer_5fchange',['os_timer_change',['../wm__os_8h.html#a8017b273c79f7fe1920e89f9d98a4428',1,'wm_os.h']]],
  ['os_5ftimer_5fcreate',['os_timer_create',['../wm__os_8h.html#a1a9321eb1259fa7355b5e7288c96a9c6',1,'wm_os.h']]],
  ['os_5ftimer_5fdeactivate',['os_timer_deactivate',['../wm__os_8h.html#a00b81d1a9cd30b2906e4c3d63de2e80e',1,'wm_os.h']]],
  ['os_5ftimer_5fdelete',['os_timer_delete',['../wm__os_8h.html#a97eca861b1fcf3dd6798d5be9b1ce6e6',1,'wm_os.h']]],
  ['os_5ftimer_5fget_5fcontext',['os_timer_get_context',['../wm__os_8h.html#a411d17e3bd2ea8196730f28c81a6582a',1,'wm_os.h']]],
  ['os_5ftimer_5fis_5frunning',['os_timer_is_running',['../wm__os_8h.html#a5e4179a9071cc72874ddfaa0ee65a72f',1,'wm_os.h']]],
  ['os_5ftimer_5freset',['os_timer_reset',['../wm__os_8h.html#aed5eed677ea22a7db9c1c27ef64aa7df',1,'wm_os.h']]],
  ['os_5ftotal_5fticks_5fget',['os_total_ticks_get',['../wm__os_8h.html#aa996a2e4eb2e2bd4f51a04d52d2fb703',1,'wm_os.h']]]
];
