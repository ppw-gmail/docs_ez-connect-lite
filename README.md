# Marvell IoT

<a href = "./mrvl_iot_banner.png"> <img src="./mrvl_iot_banner.png" width=100%> </a>

 [ Features >][html_features] [ Specs >][html_specs] [ Products >][html_products]
[html_features]: ./#understanding-ez-connect-lite-features
[html_specs]: ./#understanding-ez-connect-lite-soc-specs-hardware
[html_products]: ./#case-studies-products


EZ-Connect Lite is a SDK for Marvell Semiconductor's 88MW30x series of Wi-Fi microcontrollers.

# Quick Start Guide

This is step by step procedure helpful to quickly evaluate MW30X applications using EZ-Connect-Lite SDK

## 1. Get the Development Boards

EZ-Connect-Lite SDK supports multiple development boards from different sources, you may procure any of these for your development. SDK have built in support for all these boards. By default SDK is build for MW300_RD board (i.e. AWS IoT Starter Kit). You can always build the SDK for your desired board by passing additional build environment variable.

For Ex: to build SDK for VD-IoT-EvalKit, customise build command as described [here](./docs/eclipse/#setup-eclipse-setup-board-environment-variable)

Below is the list of supported boards for EZ-Connect-Lite SDK : -

|Name|Documentation |Link|
|:-:|:-:|
| AWS IoT Starter Kit | [More Info](./docs/starter-kit) | <a href="https://www.amazon.com/Globalscale-MW302-IoT-Starter-Powered/dp/B0168DLQHI/" target="_blank" class="button">Buy Now</a>|
| VADACTRO's VD-IoT-EvalKit | [More info](./docs/vd-iot-evalkit) |<a href="http://edu.vadactro.org.in" target="_blank" class="button">Buy Now</a>|
| Makerville Knit | [More info](./docs/makerville-knit) |<a href="https://makerville.io/knit/" target="_blank" class="button">Buy Now</a>|
| AzureWave EVB | [More info](http://www.buyiot.net/)| <a href="http://www.buyiot.net/" target="_blank" class="button">Buy Now</a>|

## 2. Setup Development Host

You will need a development host computer to work with EZ-Connect-Lite, depending upon the availablity of host at your end, you may choose any one of below procedure.

<strong>
1. [Setup Development Environement on Windows](./docs/windows-host-setup)
</strong>

<strong>
2. [Setup Development Environement on Linux](/docs/linux-host-setup)
</strong>

<strong>
3. [Setup Development Environement on Mac](/docs/mac-host-setup)
</strong>

If you are advanced Linux/Mac user more comfortable with command line interface, then you may refer a guide [Advanced Development: Command Line Interface](./docs/cli/) to work further with this SDK.

## 3. [Setup Eclipse](./docs/eclipse)
[This link documents how to setup Eclipse as a development IDE for the available development boards to work with Ez-Connect-Lite.](./docs/eclipse)

## 4. Build Sample Applications

Applications are actual your interests, using this SDK and Development board you can create any applications using the powerfull features of MW300. EZ-Connect Lite SDK contains several sample applications, that will help you to develop your own application. Few of these sample apps are explained in the [tutorial](./docs/tutorials/) section. As a part of stand build process all the supported sample apps are build. You may further [customise your build process](/docs/developing-with-the-sdk/#developing-new-application-with-ez-connect-lite-build-configuration-for-custom-app) to configure to build the sample app or custom app of your interest.

In this quick start guide we will explain the process to do a generic build and further we will use [Hello_world!](./docs/tutorials_hello_world/) sample application for flashing on the board.

Follow the steps mentioned below -

* Make sure Development Enronment host is already setup.
* Make sure Eclipse is installed and already configured setup for SDK
* Open the Eclipse IDE, if not active
* If you have multiple projects configured in the ecplipse workspace, then to to 'Project Exploer' window, then select (click on) 'ex-connect-lite' project.

<img src="./eclipse_selectproject_for_build.png" width=100%>

* Clean the project : This is optional step, but it is recommented to execute for any build after git pull or critical code changes to the SDK. In the Ecplise IDE window go to Project -> Clean
* Build the SDK with all samples apps to generate the binaries. In the Ecplise IDE window go to Project -> Build All
* You can monitor build progress in CDT build console as shown below

<img src="./eclipse_buildfinished.png" width=100%>

## 5. Flash the Binaries on the board

As a reasult of clean/build process mentioned above, we get binaries those need to be flashed on the development board. For this exercise we will use 'hello_world' appllication. Follow the steps below for the same

### External tools launchers for Eclipse
EZ-Connect-Lite has built-in external tools launchers to flash the binaries on the development board. When you setup a project on Eclipse IDE, these launchers need to be configured. This is one time setup activity for a project.

* Go to Run -> External Tools → Organize Favourites.
* Select ADD the launchers as shown below, these are launchers to flash the binaries, other two might be needed for advanced development that will be out of scope of this quick setup guide.

<img src="./eclipse_organize_ext_toolsfav.png" width=50%>

* Click on Apply to save it.
* Finally click on Ok to apply all changes.

### Prepare Flash Layout
This activity includes erasing entire flash and writing new selected flash layout for your application. Flash layout holds the flash partitioning data for different partitions needed by EZ-Connect-Lite SDK like; boot2, mcufw, ftts, wififw, etc...


* Go to Project Explorer -> ez-conect-lite and locate layout.txt in ./sdd/tools/OpenOCD/mw300/ folder, select it by clicking it.
* Go to Run -> External Tools → Prepare Flash.
* The launcher will use selected file to flash the layout on the board. on the completion on this activity, entire old data on the flash will be erased.

<img src="./docs/eclipse/eclipse_layout_flashing.png" width=100%>

### Flash BootLoader (boot2)
Bootloader is a boot firmare on the flash executed by MW300 on Power on Reset, further it reads flash layout and accodingly locates the MCU firmware in the flash. Then as per XIP/noXIP configuration it executes the application binary/firmware. Application firmware internally may use other partitions (ftfs, wififw) as per need basis.

Boot2 is a manadatory component on the flash and need to be programmed only once after fresh [Prepare Flash Layout](#quick-start-guide-5-flash-the-binaries-on-the-board-prepare-flash-layout) activity.

* Go to Project Explorer -> ez-conect-lite and locate boot2.bin in ./sdd/boot2/ folder, select it by clicking it.
* Go to Run -> External Tools → Program Boot2.
* The launcher will use selected file to flash the boot2 partition on the board flash memory.

<img src="./docs/eclipse/eclipse_boot2_flashing.png" width=100%>

### Flash Application Firmware (mcufw)
Application firmware is different for each application developed using EZ-connect-Lite. There are several sample applications available in the SDK, as well as you can develop your own, for this quick start setup we are using simple 'Hello_world" application firmware binary. Application firmware internally may use other partitions (ftfs, wififw) as per need basis.

Application firmware (mcufw) need to be reflashed to rest any or very application code changes. For each application firmware upgrade you don't need to reflash other components (like boot2 of layout flash)

* Go to Project Explorer -> ez-conect-lite and locate hello_world.bin in ./bin/mw30_rd/ folder, select it by clicking it.
* Go to Run -> External Tools → Program MCU Firmware.
* The launcher will use selected file to flash the boot2 partition on the board flash memory.

<img src="./docs/eclipse/eclipse_mcufw_flashing.png" width=100%>
* On sucessfull flashing, launcher resets the board to execute latest change.

## 6. Test the results

* Open the Minicom or Putty Serial Console on your host
* Reset the developement board by pressing "Reset" push button
* You should observe Hello_world application messages on the debug console as shown below

<img src="./minicom_helloworld_output.png" width=75%>

# Advanced Developement
### Flash layout & Partitions

#### Flash Partitions
Typical flash contents of MW300 devices are shown below:

<img src="./wmsdk_flash_layout.png" width=100%>

1. **Boot2:** This is the boot-loader. The SDK generates a boot2.bin image that acts as the boot-loader. The primary responsibility of the boot-loader is to identify the active firmware partition, and load it into the RAM. Once the image is loaded, the boot-loader jumps to the entry point of the firmware image.
If Execute-in-Place (XIP) is being used, then the boot-loader directly jumps to the entry point of the firmware image within flash. Only the data (read/write) and bss sections are loaded to RAM.
2. **Microcontroller Firmware:** This is the firmware image that primarily runs on microcontroller. When the developer builds any firmware, this is the firmware image that is generated. For example, on building the hello_world, the SDK generates a hello_world.bin file that should be stored in here.
As can be noticed, two partitions are present for the microcontroller Firmware. This is to facilitate firmware upgrades functionality. Only one of these partitions is active at a time.
During boot-up the microcontroller Firmware downloads the Wi-Fi firmware into the Wi-Fi chipset in order to initialize the Wi-Fi chipset (Optional).
3. **FTFS Filesystem:** Some applications like aws_starter_demo, may need to include a FTFS filesystem partition within the flash. This partition will contain any web-apps, or files that the Web Server running on the microcontroller can serve to browsers.
The SDK also includes support for upgrading the FTFS filesystem. If desired, developers can also create two partitions, active-passive for the FTFS upgrades.
4. **Wi-Fi Firmware:** This is the Wi-Fi firmware that is loaded into the Wi-Fi chip on boot-up. This firmware cannot be customized. It is available in a binary format along with the SDK.
5. **Persistent Configuration:** This partition holds any configuration data that should be maintained persistently. Developers are free to store their own data in this partition. This is generally used by applications like aws_starter_demo to store run time application data.

There are [External tools launchers for Eclipse](./#quick-start-guide-5-flash-the-binaries-on-the-board-external-tools-launchers-for-eclipse) that helps to flash these partitions from Exlipse IDE environment.

####Flash layout:
Default flash layout to be written to flash is stored in the file sdk/tools/OpenOCD/mw300/layout.txt. Note that there is no in-package flash in MW3XX

| Component |        Address |    Size |    device |  Name[8] |
| :------------ | :----- | :---- | :---- | :---- |
| FC_COMP_BOOT2 |     0x0    |    0x6000 |  0  |     boot2 |
| FC_COMP_PSM   |     0x6000   |  0x4000  | 0  |     psm |
| FC_COMP_FW    |     0xa000   |  0x60000 | 0  |     mcufw |
| FC_COMP_FW    |     0x6a000  |  0x60000 | 0  |     mcufw |
| FC_COMP_FTFS  |     0xca000  |  0x30000 | 0  |     ftfs |
| FC_COMP_FTFS  |     0xfa000  |  0x30000 | 0  |     ftfs |
| FC_COMP_WLAN_FW |   0x12a000 |  0x49000 | 0  |     wififw |
| FC_COMP_WLAN_FW |   0x173000 |  0x49000 | 0  |     wififw |

(***Note:*** The device = 0 represents here Flash connected on QSPI Interface of MW3XX)

If may modify a flash layout little-bit to adjust the binary sizes as per need of applications under development. The layout can be written to flash, the process is descripbed in the [Prepare Flash Layout](./#quick-start-guide-5-flash-the-binaries-on-the-board-prepare-flash-layout) section.

### API Reference Manual
Documentation about individual functions, structures, classes, and so on. This is useful to refer for developing new applications using EZ-Connect-Lite SDK.

 [ HTML Documentation >][html_api_refman] [ Download pdf >][download_api_refman]

[html_api_refman]: ./refman/html/index.html


[download_api_refman]: ./refman/pdf/sdk-reference-manual.pdf


### [Developing New Apploications](./docs/developing-with-the-sdk)
[This documentation explains the process to add new application to the EZ-Connect-Lite](./docs/developing-with-the-sdk)

### Debugging Techniques
TODO
### Command Line Interface

This is step by step procedure helpful to quickly evaluate MW30X applications using EZ-Connect-Lite SDK

TODO

# Understanding EZ-Connect-Lite

### Features

* Secure connections using TLS 1.2
* FreeRTOS as the underlying real time operating system
* Wi-Fi station and soft AP support
* lwIP open source TCP/IP stack
* MQTT client for real time communication with brokers
* Supported on Windows, Linux, OS X operating systems

### SoC Specs (Hardware)

* 32-bit ARM Cortex M4F
* Upto 200Mhz
* 512KB SRAM
* [XIP](https://en.wikipedia.org/wiki/Execute_in_place) support with external QSPI flash
* 802.11 b/g/n
* UART, SSP, I2C, PWM/GPT, ADC, DAC, and USB-OTG(only for 88MW302)
* [88MW300/302 Wi-Fi Microcontroller SoC Product details](http://www.marvell.com/microcontrollers/88MW300/302/)

### Boot-up Sequence
We saw that when the application gets compiled, we need to flash them on the board. Boot2 is a secondary boot loader. The (simplified) boot-up sequence works as:

1. Execution begins in the BootROM of the wireless microcontroller (MW3XX), which hands over control to the boot2 boot-loader, after loading it into the RAM.
2. The boot2 boot-loader looks at the partition table and determines where is the MCU firmware partition that needs to be used for execution. Once detected, the boot-loader loads this image into the RAM and hands over control to this firmware.
3. The Microcontroller Firmware is the application firmware that executes on the microcontroller. boot-loader loads and execute application frimware in RAM in non-XIP mode. In XIP mode, the boot-loader directly points the start location in the flash from where the application firmware is directly executed.

In order to execute the application firmware from the flash, following sequence of steps needs to be performed

1. [Erase flash and program the partition table](./#quick-start-guide-5-flash-the-binaries-on-the-board-prepare-flash-layout)
2. [Program boot2 image](./#quick-start-guide-5-flash-the-binaries-on-the-board-flash-bootloader-boot2)
3. [Program application firmware image](./#quick-start-guide-5-flash-the-binaries-on-the-board-flash-application-firmware-mcufw)

### SDK Architecutre

### [Tutorials](./docs/tutorials)
### SDK Downloads

 [ Download .zip >][download] or use git

    git clone https://gitlab.com/marvell-iot/ez-connect-lite


[download]: https://gitlab.com/marvell-iot/ez-connect-lite/repository/archive/master.zip



# Support

### Online Tools
- [![Join the chat at https://gitter.im/marvell-iot/ez-connect-lite](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/marvell-iot/ez-connect-lite?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
- [GitLab Issues](https://gitlab.com/groups/marvell-iot/-/issues)

### [FAQs](./docs/faqs)

[You may find answers for generic issues/problems that may occure while using EZ-Connect-Lite SDK or development boards. have a look by visting FAQs](./docs/faqs)

# Case Studies

### Projects

Here's a list of projects that people have made using the Marvell SDK.

- <a href="https://www.hackster.io/cubot/cubled-79119f" target="_blank">CUBLED</a> - Control a 4x4x4 LED cube using AWS IoT
- <a href="https://www.hackster.io/anujdeshpande/state-of-the-city-b81d85" target="_blank">State of the City</a> - Create a map with sensor data from different cloud connected sensors
- More projects on <a href="https://www.hackster.io/marvell/projects" target="_blank">Hackster</a>


### Products

Some of the commercially available products that have been made using the 88MW300/2 Wi-Fi Microcontroller-

- <a href="http://hellobarbiefaq.mattel.com/" target="_blank">Hello Barbie (USA)</a>
 - <a href="http://www.somersetrecon.com/blog/2015/11/20/hello-barbie-security-part-1-teardown" target="_blank">Teardown from Somerset Recon</a>
 - See it in action <a href="https://www.youtube.com/watch?v=RJMvmVCwoNM" target="_blank">here</a>
- <a href="http://www.mi.com/yeelight/" target="_blank">Xiaomi Yeelight (China)</a>
 - <a href="http://www.miui.com/thread-4260673-1-1.html" target="_blank">Teardown from miui.com</a>
 - See it in action <a href="https://www.youtube.com/watch?v=x0RCSBAH6gE" target="_blank">here</a>
- <a href="http://sl-bus.vadactro.org.in/" target="_blank">Smart Devices / Home Automation Products (India)</a>
 - <a href="http://www.onida.com/i-genius/" target="_blank">Ondia’s i-GENIUS Smart Air Conditioners</a>
 - <a href="http://osum.in/product.html" target="_blank">OSUM WiFi Switch</a>
 - <a href="http://smartliving.net.in/#Main-Product-Page" target="_blank">SmartLiving Home Automation Products</a>
